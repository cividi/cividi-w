FROM node:alpine

RUN apk update && \
    apk upgrade

ENV PACKAGES="\
autoconf \
automake \
bash \
build-base \
ca-certificates \
g++ \
gcc \
git \
libc6-compat \
libffi-dev \
libjpeg-turbo-dev \
linux-headers \
musl-dev \
openssl-dev \
postgresql-dev \
py3-pillow \
python3 \
python3-dev \
tzdata \
zlib-dev \
"

RUN apk add --no-cache $PACKAGES

RUN pip3 install --upgrade pip
RUN pip3 install --user psutil
RUN pip3 install rcssmin --install-option="--without-c-extensions"

# Pending 1.0.0 release
RUN pip3 install git+https://github.com/kn-id/django-yarn@master#egg=django-yarn

RUN ln -s /usr/bin/python3 /usr/local/bin/python

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip3 install --no-cache-dir -r requirements.txt

# Upgrade django-yarn (TEMPORARY)
RUN pip3 install git+https://github.com/kn-id/django-yarn@master#egg=django-yarn

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV LANG en_US.UTF-8
ENV PYTHONIOENCODING utf_8
