Cividi on Wagtail
=================

A website built using the [Wagtail CMS]https://wagtail.io/, with a [Bootstrap 4](https://getbootstrap.com/) theme packaged with [Yarn](https://yarnpkg.com/en/), fully deployable on [Docker](https://docker.com/) (with a nimble [Alpine Linux](https://hub.docker.com/r/jfloff/alpine-python/) build) and [Ansible](https://docs.ansible.com/ansible/latest/index.html) (useful for secure operations in production).

The project contains a couple of features we've developed for [customers](https://madewithwagtail.org/developers/dataletsch/), such as multilingual (German, French, English) navigation, a [Feedly](https://docs.ansible.com/ansible/latest/index.html) module for news aggregation and a customized [Puput](https://puput.readthedocs.io/en/latest/) frontend for blogging.

This project is open source under the MIT License with details in [LICENSE.md](LICENSE.md).

## Development environment

To set up a full development environment, follow the instructions starting from Frontend setup.
The easiest way to set up your machine would be to use a Docker Compose script for developers.

Rename `docker-compose.sample.yml` to `docker-compose.yml` then run:

```
make
make setup
```

If all goes well, continue to Database setup.

**Frontend setup**

Make sure a recent version of node.js (e.g. using [nave.sh](https://github.com/isaacs/nave)), then:

```
npm install -g yarn
yarn
```

The first command (`..install -g..`) may require `sudo` if you installed node.js as a system package.

**Backend setup**

If not using Vagrant: after installing Python 3, from the project folder, deploy system packages and create a virtual environment as detailed (for Ubuntu users) below:

```
sudo apt-get install python3-venv python3-dev libjpeg-dev

pyvenv env
. env/bin/activate

pip install -U pip
pip install -r requirements.txt
```

At this point your backup is ready to be deployed.

## Database setup

Once your installation is ready, you can get a blank database set up and add a user to login with. Run these commands:

```
./manage.py migrate
./manage.py createsuperuser
```

On a Docker configuration the equivalent is:

```
make migrate
make createsuperuser
```

You will be asked a few questions to create an administrator account.

**Starting up**

If you have one installed, also start your local redis server (`service redis start`).

After completing setup, you can use:

```
./manage.py runserver
```

Or the equivalent with Docker:

```
make run
```

Now access the admin panel with the user account you created earlier: http://localhost:5000/admin/ (the port may be 8000)

## Troubleshooting

- Issues with migrating database tables in SQLite during development? Try `./manage.py migrate --fake`

## Production notes

We use [Ansible](https://www.ansible.com) and [Docker Compose](https://docs.docker.com/compose/reference/overview/) for automated deployment.

To use Docker Compose to manually deploy the site, copy `ansible/roles/web/templates/docker-compose.j2` to `/docker-compose.yml` and fill in all `{{ variables }}`. This can also be done automatically in Ansible.

Install or update the following roles from [Ansible Galaxy](https://docs.ansible.com/ansible/latest/reference_appendices/galaxy.html) to use our scripts:

```
ansible-galaxy install \
   dev-sec.nginx-hardening dev-sec.ssh-hardening dev-sec.os-hardening \
   geerlingguy.certbot
```

To check that the scripts and roles are correctly installed, use this command to do a "dry run":

```
ansible-playbook ansible/*.yaml -i ansible/inventories/production --check --list-tasks
```

To do production deployments, you need to obtain SSH and vault keys from your system administrator (who has followed the Ansible guide to set up a vault..), and place these in a `.keys` folder. To deploy a site:

```
ansible-playbook -s ansible/<*.yaml> -i ansible/inventories/production
```

For an update release with a specific version, use:

```
ansible-playbook -s ansible/site.yaml -i ansible/inventories/production --tags release  -e gitversion=<v*.*.*>
```

We use a StackScript to deploy to Linode, the basic system set up is to have a user in the sudoers and docker group, and a few basic system packages ready.

For example, on Ubuntu:

```
apt-get install -q -y zip git nginx python-virtualenv python-dev
```

The order of deployment is:

- docker.yaml (base system)
- node.yaml
- site.yaml
- harden.yaml
- certbot.yaml

The last line adds support for Let's Encrypt, which you can configure and enable (updating your Nginx setup) with:

```
sudo /opt/certbot/certbot-auto --nginx certonly
```

If you do **not** wish to use SSL, delete the last part of your nginx site configuration (/etc/nginx/sites-enabled/...).

### Production releases

For further deployment and system maintenance we have a `Makefile` which automates Docker Compose tasks. This should be converted to use [Ansible Container](http://docs.ansible.com/ansible-container/getting_started.html). In the meantime, start a release with Ansible, then complete it using `make`, i.e.:

```
ansible-playbook -s ansible/site.yaml -i ansible/inventories/production --tags release
ssh -i .keys/ansible.pem ansible@<server-ip> "cd <release_dir> && make release"
```

This is already part of the normal release cycle, but if you wish to update the Docker images to the latest versions separately, use:

`make upgrade`

### Restoring a data backup

For development, it's handy to have access to a copy of the production data. To delete your local database and restore from a file backup, run:

```
rm *.sqlite3
python manage.py migrate
python manage.py loaddata my_backup_file.home.json
```

You might want to `createsuperuser` again at this point.
