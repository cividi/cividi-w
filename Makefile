export COMPOSE_FILE=./docker-compose.yml
export COMPOSE_PROJECT_NAME=cividi

default: build

upgrade:
	docker-compose pull

upgrade-build: upgrade
	docker-compose build

build:
	docker-compose build --no-cache

build-frontend:
	# execute yarn in the currently running container
	docker-compose exec web yarn

run:
	docker-compose up -d # detach by default

run-here:
	docker-compose stop web	# for restart cases, when already running
	docker-compose up

restart:
	docker-compose stop web
	docker-compose up -d web

stop:
	docker-compose stop

migrate:
	docker-compose exec web ./manage.py migrate

migrations:
	docker-compose exec web ./manage.py makemigrations --merge

apply-migrations: migrations migrate

compress:
	docker-compose exec web ./manage.py collectstatic --noinput -l -i media
	docker-compose exec web ./manage.py compress

# One ring to rule them all...
setup: run build-frontend compress migrate

createsuperuser:
	docker-compose exec web ./manage.py createsuperuser

release: compress
	docker-compose pull
	docker-compose build web
	docker-compose stop web
	docker-compose kill web
	docker-compose up -d web

reindex:
	docker-compose exec web ./manage.py update_index

clear-index:
	docker-compose exec elasticsearch curl -XDELETE localhost:9200/_all

clean:
	docker system prune --volumes --force

clean-static:
	rm -rf static/*

bash:
	# execute bash in the currently running container
	docker-compose exec web bash

run-bash:
	# run new django container, with bash, and remove it after usage
	docker-compose run --rm --no-deps web bash

django-shell:
	docker-compose exec web ./manage.py shell

logs:
	docker-compose logs -f --tail=500

backup-data:
	docker-compose exec web ./manage.py dumpdata --natural-foreign -e auth.permission -e contenttypes -e wagtailcore.GroupCollectionPermission -e wagtailimages.rendition -e sessions -e feedler.feedlysettings > ~/cividi.home.json
	zip ~/cividi.home.json.`date +"%d%m%Y-%H%M"`.zip ~/cividi.home.json
	rm ~/cividi.home.json

backup-images:
	echo "Backing up images ..."
	sudo chown -R ansible media
	zip -ruq ~/media.zip media

backup: backup-data backup-images

django-loaddata:
	gunzip ~/cividi.home.json.gz
	docker-compose exec web ./manage.py loaddata ~/cividi.home.json

restore: django-loaddata restart

psql:
	docker-compose exec postgres psql -U postgres -d postgres

pg-run-detached:
	# start pg service
	docker-compose up -d postgres

pg-exec:
	docker-compose exec postgres bash

pg-dump:
	docker-compose exec postgres bash -c 'pg_dump -U postgres -d postgres -f ./latest.sql'

pg-backup:
	docker-compose exec postgres bash -c 'pg_dump -U postgres -d postgres' > ~/pg-backup.sql
	zip ~/pg-backup.sql.`date +"%d%m%Y-%H%M"`.zip ~/pg-backup.sql
	rm ~/pg-backup.sql

pg-restore:
	docker-compose exec postgres bash -c 'psql -U postgres -d postgres -f ./latest.sql'

pg-surefire-drop-restore-db:
	# drop existing database, recreate it, and then restore its content from backup.
	docker-compose exec postgres bash -c 'dropdb -h localhost -U postgres postgres'
	docker-compose exec postgres bash -c 'createdb -h localhost -U postgres postgres'
	make pg-restore
